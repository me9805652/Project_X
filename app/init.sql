CREATE TABLE IF NOT EXISTS students (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    birth DATE NOT NULL
    );

CREATE TABLE IF NOT EXISTS subjects (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    lecturer VARCHAR(255) NOT NULL
    );

CREATE TABLE IF NOT EXISTS exams (
    id SERIAL PRIMARY KEY,
    id_lesson INT NOT NULL,
    date DATE NOT NULL,
    FOREIGN KEY (id_lesson) REFERENCES subjects(id)
    );

CREATE TABLE IF NOT EXISTS report_cards (
    id SERIAL PRIMARY KEY,
    id_student INT NOT NULL,
    id_lesson INT NOT NULL,
    mark INT,
    FOREIGN KEY (id_student) REFERENCES students(id),
    FOREIGN KEY (id_lesson) REFERENCES subjects(id)
    );


INSERT INTO students (name, surname, birth) VALUES
     ('Тимофей', 'Борисов', '2002-06-13'),
     ('Александр', 'Дорохов', '2002-07-19'),
     ('Марина', 'Сотникова',  '2002-09-25'),
     ('Лев', 'Фролов',  '2002-06-22'),
     ('Алина', 'Блинова', '2002-12-11');

INSERT INTO subjects (name, lecturer) VALUES
     ('Обработка потоковой информации интернет-вещей', 'Котилевец Игорь Денисович'),
     ('Инструментальный анализ защинённости', 'Литвин Игорь Анатольевич'),
     ('Организация обработки больших данных', 'Лебедев Артём Сергеевич'),
     ('Системы ведения хранилищ данных', 'Лебедев Артём Сергеевич'),
     ('Модели разёртывания удалённых сервисов', 'Сачков Валерий Евгеньевич');


INSERT INTO exams (id_lesson, date) VALUES
    (1, '2022-01-25'),
    (2, '2022-01-11'),
    (3, '2022-01-14'),
    (4, '2022-01-30'),
    (5, '2022-01-13');


INSERT INTO report_cards (id_student, id_lesson, mark) VALUES
    (1, 1, 5),
    (2, 1, 5),
    (3, 1, 5),
    (4, 1, 5),
    (5, 1, 5),
    (1, 2, 5),
    (2, 2, 5),
    (3, 2, 5),
    (4, 2, 5),
    (5, 2, 5),
    (1, 3, 5),
    (2, 3, 5),
    (3, 3, 5),
    (4, 3, 5),
    (5, 3, 5),
    (1, 4, 5),
    (2, 4, 5),
    (3, 4, 5),
    (4, 4, 5),
    (5, 4, 5),
    (1, 5, 5),
    (2, 5, 5),
    (3, 5, 5),
    (4, 5, 5),
    (5, 5, 5);

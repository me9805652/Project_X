import os
import psycopg2
from prettytable import PrettyTable


def conn(query):
    try:
        connection = psycopg2.connect(os.getenv("URL"))
        cursor = connection.cursor()
        return exec(cursor,  query)
    except Exception as e:
        print(e)
    finally:
        if connection:
            connection.close()
        if cursor:
            cursor.close()


def exec(cursor, query):
    cursor.execute(query)
    return cursor.fetchall()


if __name__ == '__main__':
    query = """
        SELECT name AS subject, date AS exam_date
        FROM exams
        JOIN subjects ON exams.id_lesson = subjects.id;
    """

    exam_schedule = conn(query)

    table = PrettyTable()
    table.field_names = ["Subject", "Exam Date"]

    for row in exam_schedule:
        table.add_row([row[0], row[1]])

    print(table)

import os
from forex_python.converter import CurrencyRates

def convert_rub_to_usd(amount):
    c = CurrencyRates()
    rate = c.get_rate('RUB', 'USD')
    return amount * rate

if __name__ == "__main__":
    try:
        rub_amount = float(os.getenv("RUB", 0))
        usd_amount = convert_rub_to_usd(rub_amount)
        print(f"{rub_amount} рублей равно примерно {usd_amount:.2f} долларов")
    except ValueError:
        print("Некорректный ввод. Пожалуйста, введите числовое значение.")
